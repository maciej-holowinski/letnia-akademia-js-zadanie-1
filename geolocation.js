navigator.geolocation.getCurrentPosition(success, error);

function success(position) {
    console.log(`szerokość geograficzna: ${position.coords.latitude}`)
    console.log(`długość geograficzna: ${position.coords.longitude}`)
}

function error(error) {
    if (error.code == 1) {
        console.log(`Sorki ale nie ma pozwolenia na lokalizację`);
    }
}